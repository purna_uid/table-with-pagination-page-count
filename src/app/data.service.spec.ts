import { TestBed, inject } from '@angular/core/testing';

import { DataService } from './data.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpResponse } from '@angular/common/http';

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService],
      imports: [
        HttpClientTestingModule
      ],
    });
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('should be created', () => {
    const service: DataService = TestBed.get(DataService);
    expect(service).toBeTruthy();
  });

  it('expect getSampleData() to make get call for assets/sample_data.json',
    inject([HttpTestingController, DataService],
      (httpMock: HttpTestingController, service: DataService) => {
        service.getSampleData().subscribe(data => {
          expect(data.length > 0).toBeTruthy();
        });
        const req = httpMock.expectOne('assets/sample_data.json');
        expect(req.request.method).toEqual('GET');
        req.flush([1, 2]);
      })
  );

  it('expect postData() to make post call for /api/submit',
    inject([HttpTestingController, DataService],
      (httpMock: HttpTestingController, service: DataService) => {
        const reqBody = { id: 1, status: 'read' };
        service.postData(1, 'read').subscribe(data => {
          expect(data).toBeTruthy();
        });
        const req = httpMock.expectOne('api/submit');
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toEqual(reqBody);
        const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: reqBody });
        req.flush(expectedResponse);
      })
  );

});
