import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SampleData } from './models/sample.data.model';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  public getSampleData() {
    return this.http.get<SampleData[]>('assets/sample_data.json');
  }

  public postData(id: number, status: string) {
    return this.http.post('api/submit', {id, status}, httpOptions);
  }
}
