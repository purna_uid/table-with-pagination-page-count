import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { SampleData } from '../models/sample.data.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  public sampleData: SampleData[];
  public displayData: SampleData[];
  public tableHeader: string[] = ['Name', 'Phone', 'Email', 'Company', 'Date Entry', 'Organization Number',
                                          'Address', 'Pan', 'Pin', 'Status', 'Fee', 'Guid', 'Data Exit', 'Data First',
                                          'Data Recent', 'Url', 'Post'];
  public pageLength = '10';
  pageNumbers: Array<number>;
  selectedPage: string;
  hidePagination = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getSampleData()
    .subscribe((data: SampleData[]) => {
      this.sampleData = data;
      this.loadPageNumbers();
      this.selectPage('1');
    });
  }

  public loadPageNumbers() {
    const totalPages = Math.ceil(this.sampleData.length / Number(this.pageLength));
    this.pageNumbers = [];
    for (let i = 1; i <= totalPages; i++) {
      this.pageNumbers.push(i);
    }
    this.pageLength === 'All' ? this.selectPage('All') : this.selectPage('1');
  }

  public selectPage(selectedPageNumber: string) {
    this.hidePagination = selectedPageNumber === 'All';
    this.selectedPage = selectedPageNumber;
    if (this.hidePagination) {
      this.displayData = this.sampleData;
    } else {
      this.displayData = this.sampleData.slice(((Number(selectedPageNumber) - 1) * Number(this.pageLength)),
                                                                        Number(this.pageLength) * Number(selectedPageNumber));
    }
  }

  public nextPage() {
    if (Number(this.selectedPage) === this.pageNumbers[this.pageNumbers.length - 1]) {
      return;
    }
    this.selectPage((1 + Number(this.selectedPage)).toString());
  }

  public previousPage() {
    if (Number(this.selectedPage) === this.pageNumbers[0]) {
      return;
    }
    this.selectPage((Number(this.selectedPage) - 1).toString());
  }

  public postRecord(id: number, status: string) {
    this.dataService.postData(id, status)
          .subscribe(val => console.log('data is posted'));
  }

  public updatePagination() {
    if (this.hidePagination) {
      this.pageLength = 'All';
      this.selectPage('All');
    } else {
      this.pageLength = '10';
      this.loadPageNumbers();
    }
  }

}
