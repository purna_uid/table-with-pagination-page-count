import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of as observableOf } from 'rxjs';

import { TableComponent } from './table.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from '../data.service';

class MockDataService {
  getSampleData() {
    return observableOf('');
  }

  postData(a, b) {
    return observableOf('');
  }
}

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let dataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent ],
      imports: [FormsModule, HttpClientModule],
      providers: [{provide: DataService, useClass: MockDataService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    dataService = TestBed.get(DataService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadPageNumbers() should create pagenumbers array and call selectPage() with 1st page', () => {
    spyOn(component, 'selectPage').and.returnValue();
    const sampleData: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    component.sampleData = sampleData;
    component.pageLength = '2';
    component.loadPageNumbers();
    expect(component.pageNumbers).toEqual([1, 2, 3, 4, 5, 6]);
    expect(component.selectPage).toHaveBeenCalledWith('1');
  });

  it('loadPageNumbers() should create pagenumbers array and call selectPage() with all page', () => {
    spyOn(component, 'selectPage').and.returnValue();
    const sampleData: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    component.sampleData = sampleData;
    component.pageLength = 'All';
    component.loadPageNumbers();
    expect(component.selectPage).toHaveBeenCalledWith('All');
  });

  it('selectPage() should update displayData to sampleData', () => {
    const sampleData: any = [1, 2, 3, 4, 5];
    component.sampleData = sampleData;
    component.selectPage('All');
    expect(component.hidePagination).toBeTruthy();
    expect(component.selectedPage).toEqual('All');
    expect(component.displayData).toEqual(component.sampleData);
  });

  it('selectPage() should update displayData to sliced sampleData', () => {
    const sampleData: any = [1, 2, 3, 4, 5];
    const res: any = [1, 2];
    component.sampleData = sampleData;
    component.pageLength = '2';
    component.selectPage('1');
    expect(component.hidePagination).toBeFalsy();
    expect(component.selectedPage).toEqual('1');
    expect(component.displayData).toEqual(res);
  });

  it('nextPage() should call selectPage()', () => {
    spyOn(component, 'selectPage').and.returnValue();
    component.selectedPage = '1';
    component.pageNumbers = [1, 2, 3];
    component.nextPage();
    expect(component.selectPage).toHaveBeenCalledWith('2');
  });

  it('nextPage() should not call selectPage()', () => {
    spyOn(component, 'selectPage').and.returnValue();
    component.selectedPage = '3';
    component.pageNumbers = [1, 2, 3];
    component.nextPage();
    expect(component.selectPage).not.toHaveBeenCalled();
  });

  it('previousPage() should call selectPage()', () => {
    spyOn(component, 'selectPage').and.returnValue();
    component.selectedPage = '3';
    component.pageNumbers = [1, 2, 3];
    component.previousPage();
    expect(component.selectPage).toHaveBeenCalledWith('2');
  });

  it('previousPage() should not call selectPage()', () => {
    spyOn(component, 'selectPage').and.returnValue();
    component.selectedPage = '1';
    component.pageNumbers = [1, 2, 3];
    component.previousPage();
    expect(component.selectPage).not.toHaveBeenCalled();
  });

  it('postRecord() should call dataService.postData()', () => {
    spyOn(dataService, 'postData').and.callThrough();
    component.postRecord(1, 'read');
    expect(dataService.postData).toHaveBeenCalledWith(1, 'read');
  });

  it('updatePagination() should update pageLength and call selectPage()', () => {
    spyOn(component, 'selectPage').and.returnValue();
    component.hidePagination = true;
    component.updatePagination();
    expect(component.pageLength).toEqual('All');
    expect(component.selectPage).toHaveBeenCalledWith('All');
  });

  it('updatePagination() should update pageLength and not call selectPage() and call loadPageNumbers()', () => {
    spyOn(component, 'selectPage').and.returnValue();
    spyOn(component, 'loadPageNumbers').and.returnValue();
    component.hidePagination = false;
    component.updatePagination();
    expect(component.pageLength).toEqual('10');
    expect(component.selectPage).not.toHaveBeenCalled();
    expect(component.loadPageNumbers).toHaveBeenCalled();
  });

});
